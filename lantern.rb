#!/usr/bin/env ruby

require 'thread'

#define the pins that will be used, and their muxing values
#these are:
# 9.14
# 9.16
# 8.13

@@TESTING = ARGV[0].nil? ? false : true

pins = [
	{:mux_value=>6, :mux_target=>"gpmc_a2", :pwm_dir=>"ehrpwm.1:0"},
	{:mux_value=>6, :mux_target=>"gpmc_a3", :pwm_dir=>"ehrpwm.1:1"}, 
	{:mux_value=>4, :mux_target=>"gpmc_ad9", :pwm_dir=>"ehrpwm.2:1"}
]

#make an enum type thing for the various states
WispState = [FLICKER = 'flicker', FADE = 'fade', BURST='burst', SOLID = 'solid', OFF='off']

#a class to represent an LED
class Wisp
	def initialize( pin )
		@state = "off"
		@previous_state = nil
		@min_val = 1
		@max_val = 100
		@min_sleep = 0.25
		@max_sleep = 1.25
		@min_fade = 7
		@max_fade = 50
		@current_value = @min_val
		#set the pwm_dir
		pwm_dir = File.join("/sys/class/pwm",pin[:pwm_dir])
		#set the pwm duty_percent file
		@duty_percent = File.join(pwm_dir, "duty_percent")
		@run = File.join(pwm_dir, "run" )
		#mux the target
		target = File.join("/sys/kernel/debug/omap_mux", pin[:mux_target])
		set_file_value(target, pin[:mux_value] )
		#set up the PWM
		set_file_value(@duty_percent, 0)
		set_file_value(File.join(pwm_dir,"period_freq"), 100)	
		set_file_value(@run, 1)
	end

	def info
		#return some info about this wisp
		info = {
			:state=>@state,
			:min_val => @min_val,
			:max_val => @max_val,
			:min_sleep => @min_sleep,
			:max_sleep => @max_sleep,
			:min_fade => @min_fade,
			:max_fade => @max_fade,
			:current_value => @current_value 
		}
		return info
	end

	def clean_up()
		set_percent(0)
		set_file_value(@run,0)
		@running = false
		Thread.kill(@thread) if @thread
	end 
	
	def set_percent( value )
		value = 0 if value < 0
		value = 100 if value > 100 
    begin
      set_file_value(@duty_percent, value)
      @current_value = value
    rescue
      $stdout.puts "Error writing #{value} to #{@duty_percent}"
    end
	end
	
	def flicker()
		@state = FLICKER
    kill_thread
    @thread = Thread.new do
      do_flicker
    end
	end
	
	def fade()
		@state = FADE
     kill_thread
    @thread = Thread.new do
      do_fade
    end
	end
  
	def burst()
		@state = BURST
     kill_thread
    @thread = Thread.new do
      do_burst
    end
	end
	
	def solid()
		@state = SOLID 
    kill_thread
    do_solid
	end
  
	def off()
		@state = OFF
    kill_thread
    do_off
	end

	def set_min_val(value)
		@min_val = value if value >=0 and value <=100
	end
	
	def set_max_val(value)
		@max_val = value if value >=0 and value <=100
		if @state == SOLID
			do_solid
		end
	end
	
  def kill_thread
    Thread.kill(@thread) if !@thread.nil?
  end
	
	private	
	
	def do_flicker
		min = @min_val
		max = @max_val
		current_value = @current_value
		#get a random value
    halfway = (max - min)/2 + min
    if current_value <= halfway
      val =	rand(halfway..max)
    else
      val = rand(min..halfway)
    end
    if @@TESTING
			puts "val: #{val}"
			puts "min: #{min}"
			puts "max: #{max}"
			puts "halfway: #{halfway}"
		end
		set_percent(val)
		random_sleep
		do_flicker if @state == FLICKER
	end
	
	def do_fade()
    #set attributes for this fade
    min_val = @min_val
    max_val = @max_val
    current_value = @current_value 
		val = (@current_value != min_val) ? min_val : max_val
		#get a random value
		fade_steps = rand(@min_fade..@max_fade)
		fade_steps = max_val-1 if max_val <= @max_fade
		value_step = (val - current_value)/fade_steps
		value_step = 1 if value_step == 0
		if @@TESTING
			puts "val: #{val}"
			puts "fade_steps: #{fade_steps}"
			puts "value step:	#{value_step}" 
			puts "current_value: #{current_value}"
			puts "max_val: #{max_val}"
			puts "min_val: #{min_val}"
			if value_step == 0
				puts "val: #{val}; current_value: #{current_value}; fade_steps: #{fade_steps}"
			end
		end
		
		#loop through the steps
		while current_value >= min_val and current_value <=max_val do
			set_percent(current_value)
			sleep 0.1
			current_value += value_step
		end
		if current_value < min_val
			set_percent(min_val)
		else 
			set_percent(max_val)
		end
		puts "current value", current_value
    #are we at the lowest level of fade?
    random_sleep() if current_value <= min_val
		do_fade if @state == FADE
	end
	
	def do_solid
		set_percent(@max_val)
	end
  
	def do_off
		set_percent(0)
	end
  
  def do_burst()
    set_percent(@max_val)
    #set attributes for this fade
    min_val = @min_val
    max_val = @max_val
    current_value = @current_value 
		#get a random value
		fade_steps = rand(@min_fade..@max_fade)
		fade_steps = max_val-1 if max_val <= @max_fade
		value_step = (current_value)/fade_steps
		value_step = 1 if value_step == 0
		if @@TESTING
			puts "fade_steps: #{fade_steps}"
			puts "value step:	#{value_step}" 
			puts "current_value: #{current_value}"
			puts "max_val: #{max_val}"
			puts "min_val: #{min_val}"
			if value_step == 0
				puts "val: #{val}; current_value: #{current_value}; fade_steps: #{fade_steps}"
			end
		end
		
		#loop through the steps
		while current_value >= min_val do
			set_percent(current_value)
			sleep 0.1
			current_value -= value_step
		end

    random_sleep(5) 
		do_burst if @state == BURST
	end
  

	def random_sleep(num = 1)
    num.times do
      sleep_val = rand(@min_sleep..@max_sleep)
      sleep sleep_val
    end
	end

	def set_file_value(file, value)
		if @@TESTING 
			puts "#{file} : #{value}"
			return
		end
		File.open(file, 'w') do |f|
			f.puts( value )
		end
	end

end


if $0 == __FILE__

	require 'webrick'
	require 'json'

	#make an array of wisps
	wisps = pins.each.map {|p| Wisp.new(p)}

	#run each wisp
	wisps.each do |w|
		w.off()
	end
	
	#define some server stuff
	server_port = 9477
	server_public_dir = File.join( 
		File.expand_path( File.dirname(__FILE__) ), 
		"public"
	)
	
	#create the server 
	server = WEBrick::HTTPServer.new({:Port=>server_port, :DocumentRoot=>server_public_dir})
	#what urls do we need to mount?

	server.mount_proc('/wisps') do |req, res| 
		res['Content-Type'] = 'text/json'
		res.body = JSON.dump(wisps.each.map { |w| w.info} )
	end
  
	server.mount_proc('/modes') do |req, res| 
		res['Content-Type'] = 'text/json'
		res.body = JSON.dump(WispState )
	end
	
	server.mount_proc('/change') do |req, res| 
		wisp = req.query['wisp'].to_i
		state = req.query['state']
		case state
		when "fade" 
			wisps[wisp].fade()
		when "burst" 
			wisps[wisp].burst()
		when "solid"
			wisps[wisp].solid()
		when "off"
			wisps[wisp].off()
		when "flicker"
			wisps[wisp].flicker()
		end
		#handle the max val
		max_val = req.query["max_val"]
		unless max_val.nil?
			wisps[wisp].set_max_val (max_val.to_i)
		end
		#handle the min val
		min_val = req.query["min_val"]
		unless min_val.nil?
			wisps[wisp].set_min_val (min_val.to_i)
		end
		res['Content-Type'] = 'text/json'
		res.body = JSON.dump(wisps[wisp].info )
	end
	
	trap "INT" do 
		server.shutdown 
	end
	
  #daemonize unless we are testing
  WEBrick::Daemon.start unless @@TESTING
   
	#start the server
	server.start
	
	#clean up
	wisps.each do |w|
		w.clean_up()
	end
		
end		
