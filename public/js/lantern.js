//global variables
has_modes = false; //did we get the modes from the server?
has_wisps = false; //did we get teh wisps from the server?
ui_built = false; //did the ui get built?
wisps = undefined;
modes = undefined;
//function to run on page load
$(function(){
	//get the modes
	get_modes();
	//get the wisp state
	get_wisps();
});

function get_modes() {
	var url = "/modes";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//we have the wisps!
			has_modes = true;
			modes = body;
			if (has_wisps && !ui_built) {
				ui_built = true;
				//process the data
				process_wisps_data();
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : get_wisps : "+type);
    } 
  });
}

function get_wisps() {	
	var url = "/wisps";
  $.ajax({
    url: url,
    dataType: 'json', 
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			//we have the wisps!
			has_wisps = true;
			wisps = body;
			if (has_modes && !ui_built) {
				ui_built = true;
				//process the data
				process_wisps_data();
			}
    }, 
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : get_wisps : "+type);
    } 
  });
}

function process_wisps_data() {
	var wisp_count = wisps.length;
	for (var index in wisps) {
		update_wisp_control( index, wisps[index] );
	}
}

function update_wisp_control( index, wisp ) {
	var wid = "wisp_control_"+index;
	var states = modes;
	if( $("#"+wid).length==0) {
		//create the wisp control
		var wisp_control = $("<div>");
		wisp_control.attr("id", wid);
		wisp_control.attr("class", "wisp_control");
		var title = $("<div class='title'>");
		title.html("Wisp "+(parseInt(index)+1));
		wisp_control.append(title);
		$("#wisp_controls").append(wisp_control);
		// check the state
		var radios = $("<div id='wisp_radios_"+index+"'>");
		
		for( var s_index in states ) {
			var name = states[s_index];
			var state = name.toLowerCase();
			var radio = $("<input type='radio'>");
			radio.attr("id", "radio"+state+index);
			radio.attr("name", "wisp_"+index+"_state");
			radio.attr("state", state);
			radio.attr("wisp", index);
			radio.bind('change', change_wisp_state_radio);
			var label = $("<label>");
			label.attr("for","radio"+state+index)
			label.html(name);
			radios.append(radio);
			radios.append(label);
      //is this the current state button?
      if (state == wisp['state']) {
				radio.attr('checked', true);
      }
		} 
		wisp_control.append(radios);
		radios.buttonset();
		//make a slider
		var slider = $("<div>");
		slider.attr("wisp", index);
		slider.slider({
			wisp: index,
			min: 1,
			range: true,
			values: [ wisp['min_val'], wisp['max_val'] ],
			slide: change_wisp_value
		});
		wisp_control.append(slider);
	}
}

function change_wisp_value(event, ui) {
	var wisp = $(event['target']).attr("wisp");
	var value = ui['value'];
	var values = ui['values'];
	var type=""
	if (value == values[0]) {
		type="min_val";
	} else {
		type="max_val";
	}
	//do some ajaxy shennanigans
	var url = "/change?wisp="+wisp+"&"+type+"="+value
	$.ajax({
		url: url,
		dataType: 'json', 
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		}, 
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error : update_wisp : "+type);
		} 
	});

}

function change_wisp_state_radio() {
	var radio = $(this);
	var wisp = radio.attr("wisp");
	var state = radio.attr("state");
	
	if(state!=undefined && wisp!=undefined) {
		var url = "/change?wisp="+wisp+"&state="+state
		$.ajax({
			url: url,
			dataType: 'json', 
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

			}, 
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error : update_wisp : "+type);
			} 
		});
	}
}
